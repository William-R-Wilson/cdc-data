class HomeController < ApplicationController

  YearStruct = Struct.new(:year, :total, :num_increase, :percent_increase)
  ByCauseStruct = Struct.new(:year, :all, :flu, :heart, :cancer, :cov_multiple, :cov_underlying)

  def index
    years = ["2014", "2015", "2016", "2017", "2018", "2019", "2020"]
    @totals = {}
    years.each do |year|
      @totals[year] = Week.where(year: year.to_i).sum(:all_cause)
    end
    @totals_data = []
    years.each do |year|
      diff = num_diff(year, (year.to_i-1).to_s)
      perc_diff = percent_diff(year, (year.to_i - 1).to_s)
      @totals_data.push(YearStruct.new(year, @totals[year], diff, perc_diff) )
    end
    @by_cause_data = []
    years.each do |year|
      weeks = Week.where(year: year.to_i)
      @by_cause_data.push(ByCauseStruct.new(year,
                                            weeks.sum(:all_cause),
                                            weeks.sum(:influenza_and_pneumonia),
                                            weeks.sum(:diseases_of_heart),
                                            weeks.sum(:malignant_neoplasms),
                                            weeks.sum(:covid_19_multiple_cause_of_death),
                                            weeks.sum(:covid_19_underlying_cause_of_death) )
                          )
    end
    @weeks = Week.where(year: 2020).pluck(:week).uniq!
    @weekly_totals = {"2014" => [],
                      "2015" => [],
                      "2016" => [],
                      "2017" => [],
                      "2018" => [],
                      "2019" => [],
                      "2020" => []}
    years.each do |year|
      @weeks.each do |week|
        @weekly_totals[year] << Week.where(week: week, year: year).sum(:all_cause)
      end
    end
    @weekly_totals
  end

  def num_diff(year, prev_year)
    return 0 if prev_year == "2013"

    @totals[year] - @totals[prev_year]
  end

  def percent_diff(year, prev_year)
    return 0.0 if prev_year == "2013"

    (@totals[year].to_f - @totals[prev_year].to_f) / @totals[prev_year].to_f
  end

end
