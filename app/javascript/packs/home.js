  var info = document.getElementById("chartData").dataset
  var weeks = info.weeks.split(",");
  var totals = JSON.parse(info.weeklytotals);

  function displayLineChart() {
    var lineChart = new Chart(document.getElementById("lineChart"), {
        type: 'line',
        data: {
          labels: weeks,
          datasets: [{
            data: totals["2020"],
            label: "2020 Deaths per week",
            borderColor: "#3e95cd",
            fill: false
          },
          {
            data: totals["2019"],
            label: "2019 Deaths per week",
            borderColor: "#7E7B7A",
            fill: false
          },
          {
            data: totals["2018"],
            label: "2018 Deaths per week",
            borderColor: "#7E7B6A",
            fill: false
         },
         {
          data: totals["2017"],
          label: "2017 Deaths per week",
          borderColor: "#7E7B5A",
          fill: false
          },
          {
          data: totals["2016"],
          label: "2016 Deaths per week",
          borderColor: "#7E7B8A",
          fill: false
          },
          {
          data: totals["2015"],
          label: "2015 Deaths per week",
          borderColor: "#7E7B9A",
          fill: false
          },
          {
          data: totals["2014"],
          label: "2014 Deaths per week",
          borderColor: "#7E7B7C",
          fill: false
          }
         ]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: false,
                stepSize: 1000
              }
            }]
          }
        }
    });
 }

displayLineChart();
